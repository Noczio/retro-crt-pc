# ReShade CRT custom profiles

These files comes from the original ReShade project. I wanted an easy way to share my preset with friends for retro games I play, like Sonic, Metal Slug and MegaMan.

## Instructions

* Download ReShade https://reshade.me/ and install it.
* Select default presets and the appropiate API (DX9, DX10/11/12, Vulkan).
* Copy and paste 'reshade-shaders' from 'reshade' to the game's installation folder (where the .exe is).
* Copy and paste 'ReShadePreset.ini' from 'crispy', 'blurry' or 'intermediate' to the game's installation folder (where the .exe is).

### Notes

* By default it comes with gtu and lottes v2 turned on. 
* Press Home button and open ReShade menu if you want to change any of the setting.
* Other profiles avaliable are: yeetron, zfast, royal, newpixie and lottes. Choose whichever you want and enable composite video in gtu if the game is supposed to look from that era.
* 'blurry' is only for games like Sonic mania (makes the watterfall looks as intended) and Megaman 8 (the sand in the first stage gets better color banding).

These shaders come by default when installing ReShade with CRT-Royal-ReShade by akgunter and RSRetroarch by Matsilagi, although the current repository tweaks some of the settings from my personal's favorites to use in any retro gaming from Steam/Epic. Also, these presets work with newer games, but the resolution in the ini settings should be set to 1920 x 1080 or lower, as long as it is above 960 x 540.